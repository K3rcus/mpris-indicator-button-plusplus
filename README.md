# MPRIS indicator++
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)




Fork of Mpris Indicator Button (https://github.com/JasonLG1979/gnome-shell-extension-mpris-indicator-button) with small modifications to fit my use.

If at any time the original extension stops receiving maintenance, this one will too.


 A full featured MPRIS indicator button extension for GNOME Shell 3.36+

## Like this Extension?

This extension only slightly modifies the original extension, and I will only maintain it as long as the original extension is maintained,   
so if you are considering donating the original author accept donations:

[![Flattr this git repo](https://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?user_id=JasonLG1979&url=https://gitlab.com/K3rcus/mpris-indicator-button-plusplus)

